# barreldb

A javascript implementation of Barrel-db.

THIS IS STILL IN EARLY ALPHA PHASE.

## References

The implementation is based on [levelup](https://github.com/Level/levelup)
using [fruitdown](https://github.com/nolanlawson/fruitdown) adapter.

## Build and test

    $ npm install
    $ npm test

## Run in browser

    $ npm watch

Then open http://localhost:3000/