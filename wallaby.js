// from https://github.com/wallabyjs/wallaby-browserify-sample

var wallabify = require('wallabify');
var wallabyPostprocessor = wallabify({
    // browserify options, such as
    // insertGlobals: false
  }
  // you may also pass an initializer function to chain other 
  // browserify options, such as transformers
  // , b => b.exclude('mkdirp').transform(require('babelify'))
);

module.exports = function(wallaby) {
    return {
        files: [
            { pattern: 'node_modules/chai/chai.js', instrument: false },
            { pattern: 'node_modules/jquery/dist/jquery.js', instrument: false },
            { pattern: 'source/**/*.js', load: false },
        ],

        tests: [
            { pattern: 'test/**/*-test.js', load: false }
        ],

        postprocessor: wallabyPostprocessor,

        testFramework: "mocha",

        bootstrap: function() {
            // required to trigger tests loading 
            window.__moduleBundler.loadTests();
        }
    };
};