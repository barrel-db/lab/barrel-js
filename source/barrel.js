'use strict';

var fruitdown = require('fruitdown');
var levelup = require('levelup');
var SparkMD5 = require('spark-md5');

function Barrel(dbname) {
  this._dbname = dbname;
  this._db = levelup(dbname, {db: fruitdown});
}

Barrel.prototype.put = function(doc, callback) {
  if (doc.id === undefined) {
    return callback(_error(400, 'no id provided'));
  }

  var db = this._db;
  var barrelKey = _keyDoc(doc.id);
  var options = {keyEncoding: 'json', valueEncoding: 'json'};

  db.get(barrelKey, options, function(err, value) {
    if(err) {
      if(err.status === 404) {
        // New document        
        db.put(barrelKey, doc, options, callback);
      }
      else {
        // Other error
        return callback(err);
      }
    }
    else {
      // Document already exists in database
      if(value._rev !== doc._rev) {
        return callback(_error(400, 'document conflict'));
      }
      else {
        _increaseRev(doc);
        db.put(barrelKey, doc, options, callback);
      }
    }
  });
};

Barrel.prototype.get = function(key, callback) {
  var barrelKey = _keyDoc(key);
  var options = {keyEncoding: 'json', valueEncoding: 'json'};
  this._db.get(barrelKey, options, callback);
};

Barrel.prototype.destroy = function() {
  fruitdown.destroy(this.name);
};



function _keyDoc(docid) {
  return 'barrel-ts-doc-' + docid;
}

function _increaseRev(doc) {
  var rev = doc._rev;
  var seq = 0;
  if(rev !== undefined) {
    seq = parseInt(rev.split('-')[0]);
  }
  var spark = new SparkMD5();
  spark.append(doc);
  var hexHash = spark.end();

  var newRev = (seq+1) + '-' + hexHash;
  doc._rev = newRev;
}

function _error(status, message) {
  return {status: status, message: message};
}

module.exports = Barrel;