'use strict';

function RevisionTree(tree) {
  this._revisions = tree || {};
}

RevisionTree.prototype.size = function() {
  return Object.keys(this._revisions).length;
};

RevisionTree.prototype.add = function(rev) {
  if(!rev.id) {
    throw new Error('No id defined');
  }
  if(rev.parent) {
    if(! (rev.parent in this._revisions)) {
      throw new Error('Unknown parent');
    }
  }
  this._revisions[rev.id] = rev;  
};

RevisionTree.prototype.parents = function() {
  var parents = {};
  var revisions = this._revisions;
  Object.keys(revisions).forEach(function(k) {
    var val = revisions[k];
    if (val.parent !== undefined) {
      parents[val.parent] = val;
    }
  });
  return parents;  
};

RevisionTree.prototype.leaves = function() {
  var leaves = {};
  var parents = this.parents();
  var revisions = this._revisions;
  Object.keys(revisions).forEach(function(k) {
    if (parents[k] === undefined) {
      leaves[k] = revisions[k];
    }
  });
  return leaves;  
};

RevisionTree.prototype.winner = function() {
  var leaves = this.leaves();

  var candidates = {};
  Object.keys(leaves).forEach(function(k) {
    var candidate = leaves[k];
    if(candidate.deleted !== true) {
      candidates[k] = candidate;
    }
  });

  var winner = Object.keys(candidates)[0];
  Object.keys(candidates).forEach(function(k) {
    if (isSuperior(k, winner)) {
      winner = k;
    }
  });

  return winner;
};

function isSuperior(rev1, rev2) {
  var seq1 = rev1.split('-')[0];
  var seq2 = rev2.split('-')[0];
  return seq1 > seq2;
}

module.exports = RevisionTree;