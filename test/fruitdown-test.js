/* jslint node: true, mocha:true */
'use strict';

var chai = require('chai');
var fruitdown = require('fruitdown');
var levelup = require('levelup');
var expect = chai.expect;

global.indexedDB = require('fake-indexeddb');

describe('Fruitdown', function() {

  it('use fake-indexedDB', function() {
    expect(global.indexedDB).not.undefined;
  });

  it('store and retrieve simple documents', function(done) {
    var db = levelup('destroy-test', {db: fruitdown});

    db.put('1', 4, function(err) {
      expect(err).eql(undefined);

      db.get('1', function (err2, value) {
        expect(err2).eql(null);
        expect(value).eql('4');
        done();
      });
    });
  });

  it('batch write', function(done) {
    var db = levelup('destroy-test', {db: fruitdown});

    db.batch()
      .del('father')
      .put('name', 'Yuri Irsenovich Kim')
      .put('dob', '16 February 1941')
      .put('spouse', 'Kim Young-sook')
      .put('occupation', 'Clown')
      .write(function () {
        db.get('occupation', function (err2, value) {
          expect(err2).eql(null);
          expect(value).eql('Clown');
          done();
        });
      });
  });

  it('read stream with prefix', function(done) {
    var db = levelup('prefix', {db: fruitdown});

    var values = [];

    db.batch()
      .put('/a/a', 'a1')
      .put('/b/a', 'b1')
      .put('/b/b', 'b2')
      .put('/c/a', 'c1')
      .write(function () {
        db.createReadStream({gte: '/b', lte: '/c'})
          .on('data', function (data) {
            values.push(data);
          })
          .on('error', function (err) {            
          })
          .on('close', function () {
          })
          .on('end', function () {
            expect(values.length).eql(2);
            expect(values[0].key).equal('/b/a');
            expect(values[1].key).equal('/b/b');
            done();
          });
        });
    });
  
 
});
