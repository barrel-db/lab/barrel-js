/*jslint node: true */
'use strict';

var chai = require('chai');
var Barrel = require('../source/barrel');
var expect = chai.expect;

describe('Barrel-db', function() {

  it('stores and retrieves json documents', function(done) {
    var db = new Barrel('mybarrel');
    var cat = {id: 'cat', name: 'tom'};

    db.put(cat, function(err) {
      expect(err).eql(undefined);

      db.get('cat', function (err2, doc) {
        expect(err2).eql(null);
        expect(doc.name).eql('tom');
        db.destroy();
        done();
      });
    });
  });

  it('rejects put document without id property', function(done) {
    var db = new Barrel('mybarrel');
    var doc = {name: 'tom'};

    db.put(doc, function(err, value) {
      expect(err.status).eql(400);
      db.destroy();
      done();
    });
  });

  it('rejects put document with wrong revision', function(done) {
    var db = new Barrel('mybarrel');
    var cat = {id: 'cat', name: 'tom'};

    db.put(cat, function(err1, value) {
      expect(err1).eql(undefined);
      var cat2 = {id: 'cat', name: 'tom'};
      db.put(cat2, function(err2, value) {
        expect(err2.status).eql(400);
        db.destroy(); 
        done();
      });
    });  
  });

});