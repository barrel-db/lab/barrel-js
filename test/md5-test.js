/*jslint node: true */
'use strict';
var chai = require('chai');
var expect = chai.expect;
var SparkMD5 = require('spark-md5');

describe('Spark-md5', function() {

  it('computes md5', function() {
    var spark = new SparkMD5();
    spark.append('hello');
    spark.append('world');
    var hexHash = spark.end();
    expect(hexHash).to.equal('fc5e038d38a57032085441e7fe7010b0');
  });
 
});
