/*jslint node: true */
'use strict';

var chai = require('chai');
var RevisionTree = require('../source/revision-tree');
var expect = chai.expect;

describe('RevisionTree', function() {
  
  //                  3-three
  //                /
  // 1-one -> 2-two
  //                \
  //                  3-three-2
  var banchedTree = {
    '1-one':     { 'id': '1-one' },
    '2-two':     { 'id': '2-two',     'parent': '1-one' },
    '3-three':   { 'id': '3-three',   'parent': '2-two' },
    '3-three-2': { 'id': '3-three-2', 'parent': '2-two' }
  };

  it('is created empty', function() {
    var t = new RevisionTree();
    expect(t.size()).to.equal(0);   
  });

  it('accept new revision', function() {
    var t = new RevisionTree();
    t.add({'id': '1-one'});    
    expect(t.size()).to.equal(1);
    expect(function() {t.add({'noid': 'hello'});}).to.throw('No id defined'); 
    expect(function() {t.add({'id': '2-two', 'parent': 'unknown'});}).to.throw('Unknown parent'); 
    expect(function() {t.add({'id': '2-two', 'parent': '1-one'});}).to.not.throw(); 
  });

  it('can list the parents', function() {
    var t = new RevisionTree(banchedTree);
    var parents = t.parents();
    var size = Object.keys(parents).length;
    expect(size).to.equal(2);
    expect(parents).to.have.all.keys(['1-one', '2-two']);
  });

  it('can list the leaves', function() {
    var t = new RevisionTree(banchedTree);
    var leaves = t.leaves();
    var size = Object.keys(leaves).length;
    expect(size).to.equal(2);
    expect(leaves).to.have.all.keys(['3-three', '3-three-2']);
  });

  it('propose the most recent branch as winning', function() {
    var t = new RevisionTree(banchedTree);
    expect(t.winner()).to.equal('3-three');
  });

});
