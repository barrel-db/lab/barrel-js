/*jslint node: true */
'use strict';

var chai = require('chai');
var revision = require('../source/revision');
var expect = chai.expect;

describe('Revision', function(done) {

  it('computes a revision id', function() {
       
    var doc = {id: 'cat', name: 'tom'};
    doc = revision.rev(doc);
    
    expect(doc._rev).to.equal('hello');
  });

  

});
