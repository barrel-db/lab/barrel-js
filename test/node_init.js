'use strict';

// use fake-indexedDB when running from Node-JS
if (typeof process !== 'undefined' && !process.browser) {
  global.indexedDB = require('fake-indexeddb');
}
