'use strict';

//******************************************************************************
//* DEPENDENCIES
//******************************************************************************
var gulp        = require('gulp'),
    jshint      = require('gulp-jshint'),
    browserify  = require('browserify'),
    source      = require('vinyl-source-stream'),
    buffer      = require('vinyl-buffer'),
    sourcemaps  = require('gulp-sourcemaps'),
    uglify      = require('gulp-uglify'),
    runSequence = require('run-sequence'),
    mocha       = require('gulp-mocha'),
    istanbul    = require('gulp-istanbul'),
    browserSync = require('browser-sync').create();
    
//******************************************************************************
//* LINT
//******************************************************************************
gulp.task('lint', function() {   
    return gulp.src([
        'source/**/**.js',
        'test/**/**-test.js'
    ])
    .pipe(jshint());
});

//******************************************************************************
//* TEST
//******************************************************************************
gulp.task('istanbul:hook', function() {
    return gulp.src(['source/**/*.js'])
        // Covering files
        .pipe(istanbul())
        // Force `require` to return covered files
        .pipe(istanbul.hookRequire());
});

gulp.task('test', ['istanbul:hook'], function() {
    return gulp.src(['test/node_init.js', 'test/**/*-test.js'])
        .pipe(mocha({ui: 'bdd'}))
        .pipe(istanbul.writeReports());
});

//******************************************************************************
//* BUNDLE
//******************************************************************************
gulp.task('bundle', function() {
  
    var libraryName = 'barrel-db';
    var mainFilePath = 'source/main.js';
    var outputFolder   = 'dist/';
    var outputFileName = libraryName + '.min.js';

    var bundler = browserify({
        debug: true,
        standalone : libraryName
    });
    
    return bundler.add(mainFilePath)
        .bundle()
        .pipe(source(outputFileName))
        .pipe(buffer())
        .pipe(sourcemaps.init({ loadMaps: true }))
        .pipe(uglify())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(outputFolder));
});


//******************************************************************************
//* CONTINUOUS TESTING
//******************************************************************************
gulp.task('watch', function () {  
    gulp.watch(['source/**/*.js', 'test/**/*-test.js'], function() {
        return gulp.src(['test/**/*-test.js'])
            .pipe(mocha({ui: 'bdd'}));
    });
});

//******************************************************************************
//* DEV SERVER
//******************************************************************************
gulp.task('server', ['default'], function () {
    
    browserSync.init({
        server: '.'
    });
    
    gulp.watch(['source/**/**.js', 'test/**/*.js'], ['default']);
    gulp.watch('dist/*.js').on('change', browserSync.reload); 
});

//******************************************************************************
//* DEFAULT
//******************************************************************************
gulp.task('default', function (cb) {
    runSequence('lint', 'test', 'bundle', cb);
});